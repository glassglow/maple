#ifndef MAPLE_RENDER_H
#define MAPLE_RENDER_H

void maple_init_render(void);
void maple_quit_render(void);

void maple_set_render_size(int width, int height);

void maple_render_frame(void);

#endif /* MAPLE_RENDER_H */
