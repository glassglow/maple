#include "application.h"

#include <GLFW/glfw3.h>

#include "log.h"
#include "render.h"
#include "status.h"
#include "window.h"

static int maple_glfw_initialized = 0;

int maple_init_application(void) {
  int status;

  maple_init_log();

  if(glfwInit() != GL_TRUE) {
    return MAPLE_STATUS_GLFW_ERROR;
  }
  maple_glfw_initialized = 1;

  status = maple_init_window();
  if(status != MAPLE_STATUS_SUCCESS) {
    return status;
  }

  return MAPLE_STATUS_SUCCESS;
}

int maple_exec_application(void) {
  while(maple_window_is_open()) {
    maple_render_frame();
    glfwPollEvents();
  }
  return MAPLE_STATUS_SUCCESS;
}

int maple_quit_application(void) {
  maple_quit_window();
  if(maple_glfw_initialized) {
    glfwTerminate();
  }
  maple_quit_log();
  return MAPLE_STATUS_SUCCESS;
}
