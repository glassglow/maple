#include <stdio.h>
#include <stdlib.h>

#include "application.h"
#include "status.h"
#include "log.h"

int main(void) {
  int status;
  int exit_code;

  exit_code = EXIT_SUCCESS;

  status = maple_init_application();
  if(status != MAPLE_STATUS_SUCCESS) {
    maple_log_status(status);
    exit_code = EXIT_FAILURE;
  } else {
    status = maple_exec_application();
    if(status != MAPLE_STATUS_SUCCESS) {
      maple_log_status(status);
      exit_code = EXIT_FAILURE;
    }
  }

  status = maple_quit_application();
  if(status != MAPLE_STATUS_SUCCESS) {
    maple_log_status(status);
    exit_code = EXIT_FAILURE;
  }

  return exit_code;
}
