#ifndef MAPLE_APPLICATION_H
#define MAPLE_APPLICATION_H

int maple_init_application(void);
int maple_exec_application(void);
int maple_quit_application(void);

#endif
