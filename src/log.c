#include "log.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <GLFW/glfw3.h>

#include "status.h"

static char *maple_glfw_error = NULL;

static void maple_glfw_error_callback(
  int error,
  char const *description
) {
  int len;

  (void) error;

  if(maple_glfw_error != NULL) {
    free(maple_glfw_error);
  }

  len = strlen(description);
  maple_glfw_error = malloc(len * sizeof(char));
  strcpy(maple_glfw_error, description);
}

void maple_init_log(void) {
  glfwSetErrorCallback(maple_glfw_error_callback);
}

void maple_quit_log(void) {
  if(maple_glfw_error != NULL) {
    free(maple_glfw_error);
  }
}

void maple_log_status(int status) {
  char const *class;
  char const *message;

  switch(status) {
    case MAPLE_STATUS_SUCCESS:
      class = "info";
      message = "operation succeeded";
      break;

    case MAPLE_STATUS_GLFW_ERROR:
      class = "error";
      message = maple_glfw_error == NULL
        ? "unknown GLFW error"
        : maple_glfw_error;
      break;
  }

  fprintf(stderr, "maple: %s: %s\n", class, message);
}
