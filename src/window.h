#ifndef MAPLE_WINDOW_H
#define MAPLE_WINDOW_H

int maple_init_window(void);
int maple_quit_window(void);

int maple_window_is_open(void);
void maple_window_swap_buffers(void);

#endif /* MAPLE_WINDOW_H */
