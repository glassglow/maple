#ifndef MAPLE_LOG_H
#define MAPLE_LOG_H

void maple_init_log(void);
void maple_quit_log(void);

void maple_log_status(int status);

#endif
