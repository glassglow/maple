#include "window.h"

#include <GLFW/glfw3.h>
#include "status.h"
#include "render.h"

static GLFWwindow *maple_glfw_window = NULL;

int maple_init_window(void) {
  maple_glfw_window = glfwCreateWindow(1280, 720, "Maple", NULL, NULL);
  if(maple_glfw_window == NULL) {
    return MAPLE_STATUS_GLFW_ERROR;
  }
  glfwMakeContextCurrent(maple_glfw_window);
  maple_init_render();
  maple_set_render_size(1280, 720);
  return MAPLE_STATUS_SUCCESS;
}

int maple_quit_window(void) {
  maple_quit_render();
  if(maple_glfw_window != NULL) {
    glfwDestroyWindow(maple_glfw_window);
  }
  return MAPLE_STATUS_SUCCESS;
}

int maple_window_is_open(void) {
  return !glfwWindowShouldClose(maple_glfw_window);
}

void maple_window_swap_buffers(void) {
  glfwSwapBuffers(maple_glfw_window);
}
