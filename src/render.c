#include "render.h"

#include <epoxy/gl.h>
#include <GLFW/glfw3.h>

#include "window.h"

static float maple_half_w = 0.0f;
static float maple_half_h = 0.0f;

void maple_init_render(void) {
  glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
}

void maple_quit_render(void) {
}

void maple_set_render_size(int width, int height) {
  glViewport(0, 0, width, height);

  maple_half_w = width / 2.0f;
  maple_half_h = height / 2.0f;

  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glOrtho(
    -maple_half_w, maple_half_w,
    -maple_half_h, maple_half_h,
    -1.0f, 1.0f
  );
}

void maple_render_frame(void) {
  glClear(GL_COLOR_BUFFER_BIT);
  maple_window_swap_buffers();
}
