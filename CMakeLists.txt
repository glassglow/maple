cmake_minimum_required(VERSION 3.5)

project(maple VERSION 1.0 LANGUAGES C)

include_directories(./dep/inc)
link_directories(./dep/lib)

set(CMAKE_C_FLAGS "${CMAKE_CX_FLAGS} -std=c89 -Wall -Wextra -Wpedantic -Werror")
set(CMAKE_C_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -g")

add_subdirectory(./src)
